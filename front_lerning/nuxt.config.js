import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: 'STB888 | %s',
    title: 'บาคาร่า คาสิโนสด เสือมังกร สล็อตออนไลน์',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: ' บาคาร่า คาสิโนสด เสือมังกร สล็อตออนไลน์', name: ' บาคาร่า คาสิโนสด เสือมังกร สล็อตออนไลน์', content: ' บาคาร่า คาสิโนสด เสือมังกร สล็อตออนไลน์' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo-stb888.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/gogle-al' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/fontawesome',
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/bootstrap-vue',
    '@nuxtjs/axios'

  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      options: {
        customProperties: true
      },
      dark: true,
      themes: {
        dark: {
          background: '#091725'
        },
        light: {
          background: '#d0f0c0'
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  axios: {
    proxy: true, // Can be also an object with default options

  },

  proxy: {
    // Simple proxy
    // '/api': 'http://178.128.21.58:5700',
    // '/todos': 'https://jsonplaceholder.typicode.com',
    // '/mountains': 'https://api.nuxtjs.dev',
    // '/planetary': 'https://api.nasa.gov',
    // // '/ghost': 'http://ghost:2368',
    // '/ghost': 'http://localhost:3001',

    // http://localhost:3001/ghost/api/v3/content/posts/?key=6f23bfc104035252692ca7121e
  },
  server: {
    host: "0.0.0.0",
    port: process.env.PORT_WEBSITE || 80
  }

}

